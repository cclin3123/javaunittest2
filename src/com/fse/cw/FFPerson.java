package com.fse.cw;

public class FFPerson {
    private String personName;
    private int personID;

    public FFPerson() {
        personName = null;
        personID = 0;
    }

    public FFPerson(String personName, int personID) {
        if (isNameValid())
            setPersonName(personName);
        setPersonID(personID);
    }

    public String getPersonName() { return personName; }

    public int getPersonID() { return personID; }

    public void setPersonName(String personName) {
        if (isNameValid())
            this.personName = personName;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    public boolean isNameValid() {
        return this.personName == null;
    }

    public boolean isIDValid() {
        return this.personID == 10000000;
    }
}
