package com.fse.cw;

import java.util.ArrayList;
import java.util.Date;

public class FFTeam {
    private ArrayList<FFPerson> teamList;
    private int teamLeader;
    private Date createTime;
    
    public FFTeam() {
        teamList = null;
        teamLeader = 0;
        createTime = new Date();
    }

    public void setTeamList(ArrayList<FFPerson> teamList) {
        if (this.teamList == null)
            this.teamList = teamList;
    }

    public void setTeamLeader(int teamLeader) {
        this.teamLeader = teamLeader;
    }

    public void addTeamMemberToList(FFPerson teamMember) {
        this.teamList.add(teamMember);
    }

    public Date getCreateTime() {
        return createTime;
    }

    public int getTeamLeader() {
        return teamLeader;
    }

    public ArrayList<FFPerson> getTeamList() {
        return teamList;
    }

    @Override
    public String toString() {
        String leader = "Team Leader: " + getTeamList().get(getTeamLeader()).getPersonName() + "\n";
        String member = "Team Members:" + "\n";
        for (int i = 0; i < getTeamList().size(); i++) {
            member += "[" + i + "]: " + getTeamList().get(i).getPersonName() + "\n";
        }
        String date = "Created Date and Time: " + getCreateTime().toString();

        return leader + member + date;
    }
}
