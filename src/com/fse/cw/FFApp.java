package com.fse.cw;

import java.util.ArrayList;
import java.util.Scanner;

public class FFApp {
    public static ArrayList<FFTeam> fireTeams;

    public static void main(String[] args) {
        fireTeams = new ArrayList<FFTeam>();

        // Setup Sample Team 1
        ArrayList<FFPerson> team1M = new ArrayList<FFPerson>();
        team1M.add(new FFPerson("Hawkeye", 20220101));
        team1M.add(new FFPerson("Black Widow", 20220801));
        team1M.add(new FFPerson("Iron Man", 20229756));
        team1M.add(new FFPerson("Thor", 20228842));
        team1M.add(new FFPerson("Captain America", 20220808));
        FFTeam team1 = new FFTeam();
        team1.setTeamLeader(4);
        team1.setTeamList(team1M);
        fireTeams.add(team1);

        // Setup Sample Team 2
        ArrayList<FFPerson> team2M = new ArrayList<FFPerson>();
        team2M.add(new FFPerson("Batman", 20224477));
        team2M.add(new FFPerson("Superman", 20229988));
        team2M.add(new FFPerson("Wonder Woman", 20223378));
        team2M.add(new FFPerson("Flash", 20221335));
        FFTeam team2 = new FFTeam();
        team2.setTeamLeader(1);
        team2.setTeamList(team2M);
        fireTeams.add(team2);

        DisplayMainMenu();
        Scanner in = new Scanner(System.in);
        while(in.hasNextLine()) {
            String choice = in.next();
            try {
                if (choice.equals("D")) {
                    DisplayAllTeams();
                } else if (choice.equals("S")) {
                    SearchFirefighter();
                } else if (choice.equals("T")) {
                    AddTeam();
                } else if (choice.equals("F")) {
                    AddFirefighter();
                } else if (choice.equals("L")) {
                    ChangeTeamLeader();
                } else if (choice.equals("M")) {
                    ChangeTeamMember();
                } else if (choice.equals("X")) {
                    System.out.println("\nGoodbye!");
                    break;
                }
            } catch(Exception e) {
                System.out.println("Something went wrong: " + e.toString() + "\n");
            }

            DisplayMainMenu();
        }
        in.close();
    }

    public static void DisplayMainMenu() {
        System.out.println("\nWhat do you want to do?");
        System.out.println("[D]: Display firefighter teams");
        System.out.println("[S]: Search for a firefighter.");
        System.out.println("[T]: Create a new team with a team leader.");
        System.out.println("[F]: Enter new firefighter.");
        System.out.println("[L]: Change team leader.");
        System.out.println("[M]: Change team member.");
        System.out.println("[X]: Exit.");
        System.out.print("Enter choice: ");
    }

    public static void DisplayAllTeams() {
        for (int j = 0; j < fireTeams.size(); j++) {
            FFTeam fireTeam = fireTeams.get(j);
            System.out.println("\nTeam " + (j + 1));
            System.out.println("------");
            System.out.println(fireTeam);
        }
    }

    public static void SearchFirefighter() {
        System.out.print("Enter firefighter name: ");
        Scanner in = new Scanner(System.in);
        String name = in.nextLine();

        for (FFTeam fireTeam : fireTeams) {
            FFPerson teamLeader = fireTeam.getTeamList().get(fireTeam.getTeamLeader());

            if (teamLeader.getPersonName().equals(name)) {
                System.out.println(name + " is the leader of a team.");
                return;
            }

            for (FFPerson member : fireTeam.getTeamList()) {
                if (member.getPersonName().equals(name)) {
                    System.out.println(name + " is the member of the team.");
                    return;
                }
            }
        }

        System.out.println(name + " is not exist.");
    }

    public static void AddTeam() {
        Scanner in = new Scanner(System.in);

        System.out.print("\nEnter team leader name: ");
        String name = in.nextLine();
        System.out.print("Enter team leader ID: ");
        String id = in.nextLine();

        ArrayList<FFPerson> teamM = new ArrayList<>();
        teamM.add(new FFPerson(name, Integer.parseInt(id)));
        FFTeam team = new FFTeam();
        team.setTeamLeader(0);
        team.setTeamList(teamM);
        fireTeams.add(team);

        System.out.println("Team created successfully.");
    }

    public static void AddFirefighter() {
        Scanner in = new Scanner(System.in);

        System.out.print("\nEnter firefighter name: ");
        String name = in.nextLine();
        System.out.print("Enter team leader ID: ");
        String id = in.nextLine();

        System.out.println("----------");
        for (int j = 0; j < fireTeams.size(); j++) {
            FFTeam fireTeam = fireTeams.get(j);
            FFPerson teamLeader = fireTeam.getTeamList().get(fireTeam.getTeamLeader());

            System.out.println("Team " + (j + 1) + " [" + teamLeader.getPersonName() + "]");
        }
        System.out.println("----------");
        System.out.print("Which team do you want to assign to?: ");
        int index = in.nextInt();

        FFPerson firefighter = new FFPerson(name, Integer.parseInt(id));
        fireTeams.get(index - 1).addTeamMemberToList(firefighter);

        System.out.println("Firefighter created successfully and added to team " + index + ".");
    }

    public static void ChangeTeamLeader() {
        System.out.println("\n----------");
        for (int j = 0; j < fireTeams.size(); j++) {
            FFTeam fireTeam = fireTeams.get(j);
            FFPerson teamLeader = fireTeam.getTeamList().get(fireTeam.getTeamLeader());

            System.out.println("Team " + (j + 1) + " [" + teamLeader.getPersonName() + "]");
        }
        System.out.println("----------");

        Scanner in = new Scanner(System.in);

        System.out.print("Which team leader to change?: ");
        int index = in.nextInt();

        System.out.println("\n----------");
        System.out.println("Team Members:");
        FFTeam fireTeam = fireTeams.get(index - 1);
        for (int i = 0; i < fireTeam.getTeamList().size(); i++) {
            System.out.println("[" + (i + 1) + "]: " + fireTeam.getTeamList().get(i).getPersonName());
        }
        System.out.println("----------");

        System.out.print("Who is the new team leader?: ");
        index = in.nextInt();

        fireTeam.setTeamLeader(index);

        System.out.println("Team leader has changed successfully.");
    }

    public static void ChangeTeamMember() {
        System.out.println("\n----------");
        for (int j = 0; j < fireTeams.size(); j++) {
            FFTeam fireTeam = fireTeams.get(j);
            FFPerson teamLeader = fireTeam.getTeamList().get(fireTeam.getTeamLeader());

            System.out.println("Team " + (j + 1) + " [" + teamLeader.getPersonName() + "]");
        }
        System.out.println("----------");

        Scanner in = new Scanner(System.in);

        System.out.print("Which team leader to change?: ");
        int index = in.nextInt() - 1;

        System.out.println("\n----------");
        System.out.println("Team Members:");
        FFTeam fireTeam = fireTeams.get(index);
        for (int i = 0; i < fireTeam.getTeamList().size(); i++) {
            System.out.println("[" + (i + 1) + "]: " + fireTeam.getTeamList().get(i).getPersonName());
        }
        System.out.println("----------");

        System.out.print("Which team member to move?: ");
        int memberIndex = in.nextInt() - 1;

        System.out.println("\n----------");
        for (int j = 0; j < fireTeams.size(); j++) {
            if (index != j) {
                fireTeam = fireTeams.get(j);
                FFPerson teamLeader = fireTeam.getTeamList().get(fireTeam.getTeamLeader());

                System.out.println("Team " + (j + 1) + " [" + teamLeader.getPersonName() + "]");
            }
        }
        System.out.println("----------");
        System.out.print("Which team to move to?: ");
        int teamIndex = in.nextInt() - 1;

        fireTeams.get(teamIndex).getTeamList().add(fireTeams.get(index).getTeamList().get(memberIndex));
        fireTeams.get(index).getTeamList().remove(memberIndex);

        System.out.println("Team member has changed successfully.");
    }
}
