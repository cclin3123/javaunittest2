# Test Plan (and eventually Test Report) - Example Template

|Contents|
|--------|
|[Team Management](#team-management)|
|[Test Plan](#test-plan)|
|[FFTeam](#class)|
|[Jump to Class:](#class)|
|[Jump to Class:](#class)|


## Team Management
Report here, by the end of the assignment, how the team has managed the project, e.g.: used version control, organised meetings, divided work, used labels, milestones, issues, etc. etc.

## Test Plan
**You should add rows and even columns, and indeed more tables, freely as you think will improve the report.**

### Class: FFPerson

#### Function: getPersonName()

For testing purpose, we will crate a team FFPerson("Linus Lin",...) for testing.

|Test|Inputs|Expected Outcome|Test Outcome|Result|
|----|------|----------------|------------|------|
|0001|ffPerson2.setPersonName("Linus Lin")  |Linus Lin          |      | FAILED |
|0002|ffPerson2.setPersonName("Linus_Lin@!X")  |Linus Lin          |      | FAILED |

#### Function:

|Test|Inputs|Expected Outcome|Test Outcome|Result|
|----|------|----------------|------------|------|
| | | |
| | | |

### Class:

#### Function:

|Test|Inputs|Expected Outcome|Test Outcome|Result|
|----|------|----------------|------------|------|
| | | |
| | | |

#### Function:

|Test|Inputs|Expected Outcome|Test Outcome|Result|
|----|------|----------------|------------|------|
| | | |
| | | |

