package com.fse.cw;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class FFPersonTest {
	private static FFPerson ffPerson = new FFPerson();
	private static FFPerson ffPerson2 = new FFPerson("Linus Lin", 12345678);
	
    @Test
    void testFFPerson() {
        assertEquals(0, ffPerson.getPersonID(), "The number should be 0");
        // assertNull(ffPerson.getPersonName());
        assertEquals(12345678, ffPerson2.getPersonID());
        //ffPerson2.setPersonName("CC Lin");
        //Test setPersonName, test case:001
        ffPerson.setPersonName("Linus Lin");
        assertEquals("Linus Lin", ffPerson.getPersonName());
      //Test setPersonName, test case:002
        ffPerson.setPersonName("Linus_Lin@@!!!");
        assertEquals("Linus Lin", ffPerson.getPersonName());
    }
}