package com.fse.cw;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class FFTeamTest {
	
	ArrayList<FFPerson> team1M = new ArrayList<FFPerson>();
    team1M.add(new FFPerson("Hawkeye", 20220101));
    team1M.add(new FFPerson("Black Widow", 20220801));
    team1M.add(new FFPerson("Iron Man", 20229756));
    team1M.add(new FFPerson("Thor", 20228842));
    team1M.add(new FFPerson("Captain America", 20220808));
    FFTeam team1 = new FFTeam();
    team1.setTeamLeader(4);
    team1.setTeamList(team1M);
	
    @Test
    void testFFPerson() {
        assertEquals(team1M, team1.getTeamList());
        // assertNull(ffPerson.getPersonName());
        //assertEquals(12345678, ffPerson2.getPersonID());
        //assertEquals("Linus Lin", ffPerson2.getPersonName());
    }
}